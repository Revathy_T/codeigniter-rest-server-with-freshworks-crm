<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/RestController.php';
require_once("application/libraries/RestController.php");
require_once("application/libraries/Format.php");

use chriskacerguis\RestServer\RestController;

class ApiDemoController extends RestController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('contactModel');
    }
    public function index_get()
    {
      $contacts= new contactModel;
      $result= $contacts->get_contact();
      $this->response($result,200);
        //echo "I am Revathy";
    }
    public function index_post(){
      $contacts= new contactModel;
      $data_store=$this->input->post('data_store');
      if($data_store==1){
        $data = array('first_name' => $this->input->post('first_name'),
        'last_name' => $this->input->post('last_name'),
        'mobile_number' => $this->input->post('mobile_number'),
        'email' => $this->input->post('email'),
        'owner_id'=>array('30000834533')
        );
        $r = $contacts->insert($data);
      }else{
        $data = array('first_name' => $this->input->post('first_name'),
        'last_name' => $this->input->post('last_name'),
        'mobile_number' => $this->input->post('mobile_number'),
        'email' => $this->input->post('email')
        );
        $r=$contacts->insertDb($data);
      }
        $this->response($r); 
    }
    public function index_put(){
       $id=$this->input->get('id');
      $contacts= new contactModel;
      $data = array('mobile_number' => $this->input->get('mobile'),
            );
      $data_store=$this->input->get('data_store');
      if($data_store==1){
       $r = $contacts->update($id,$data);
      }else{
       $r = $contacts->updateDb($id,$data);

      }
       $this->response($r); 
    }
    public function index_delete(){
      $id = $this->input->get('id');
      $contacts= new contactModel;
      $data_store=$this->input->get('data_store');
      if($data_store==1){
      $r = $contacts->delete($id);
      }else{
      $r = $contacts->deleteDb($id);

      }
      $this->response($r); 
  }
}

?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class ContactModel extends CI_Model
{
    
    public function get_contact()
    {
       $query= $this->db->get('contact');
        $arr1= $query->result_array(); 
        //print_r($arr1);
       $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://krify.myfreshworks.com/crm/sales/api/contacts/view/30000834530',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'Authorization: Token token=rLijPKdPiKQL4ThvMi9Wpw',
            'Content-Type: application/json'
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $arr2=json_decode($response,true);
        //print_r($arr2);
        $res=array_merge($arr1,$arr2['contacts']);
        return ($res);
    }
    //return 0;
    public function create_contact(){
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://krify.myfreshworks.com/crm/sales/api/contacts/view/30000834533',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'Authorization: Token token=rLijPKdPiKQL4ThvMi9Wpw',
            'Content-Type: application/json'
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return json_decode($response,true);


    }
    public function insertDb($data){
       
        $this->first_name    = $data['first_name']; // please read the below note
        $this->last_name  = $data['last_name'];
        $this->mobile_number = $data['mobile_number'];
        $this->email = $data['email'];
        $this->db->where('email', $this->email);

        $query = $this->db->get('contact');

        $count_row = $query->num_rows();

        if ($count_row > 0) {
            return 'Email Id already exists';
        } 
        if($this->db->insert('contact',$this))
        {    
            return 'Data is inserted successfully';
        }
          else
        {
            return "Error has occured";
        }
    }
    public function insert($data)
    {
        $arr['contact']=$data;
        $input= json_encode($arr);
        $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, 'https://krify.myfreshworks.com/crm/sales/api/contacts');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $input);

            $headers = array();
            $headers[] = 'Authorization: Token token=rLijPKdPiKQL4ThvMi9Wpw';
            $headers[] = 'Content-Type: application/json';
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            }
            curl_close($ch);
            return json_decode($result,true);
    }
    public function updateDb($id,$data){
   
        $this->mobile_number = $data['mobile_number'];
        //$this->email = $data['email'];
        // $this->db->where('email', $this->email);

        // $query = $this->db->get('contact');

        // $count_row = $query->num_rows();

        // if ($count_row > 0) {
        //     return 'Email Id already exists';
        // }
         $result = $this->db->update('contact',$this,array('user_id' => $id));
         if($result)
         {
             return "Data is updated successfully";
         }
         else
         {
             return "Error has occurred";
         }
     }
  

    public function update($id,$data)
    {
        $arr['contact']=$data;
        $input= json_encode($arr);
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://krify.myfreshworks.com/crm/sales/api/contacts/'.$id);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');

        curl_setopt($ch, CURLOPT_POSTFIELDS, $input);

        $headers = array();
        $headers[] = 'Authorization: Token token=rLijPKdPiKQL4ThvMi9Wpw';
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        return json_decode($result,true);

    }
    public function delete($id)
    {
        
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://krify.myfreshworks.com/crm/sales/api/contacts/'.$id);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        $headers = array();
        $headers[] = 'Authorization: Token token=rLijPKdPiKQL4ThvMi9Wpw';
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        return json_decode($result,true);

    }
    public function deleteDb($id){
   
        $result = $this->db->query("delete from `contact` where user_id =". $id);
        if($result)
        {
            return "Data is deleted successfully";
        }
        else
        {
            return "Error has occurred";
        }
    }

}
?>